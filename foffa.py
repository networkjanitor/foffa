
# Foffa is a utility programm for text commanding GuildWars2.
# Copyright (C) 2017  Networkjanitor Netsphere
#
# This file is part of Foffa.
#
# Foffa is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Foffa is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Foffa. If not, see <http://www.gnu.org/licenses/>.

import sys
from PyQt5.QtWidgets import QApplication, QMainWindow
from qtgui.mw import Ui_MainWindow
import qtgui.qw

def main():
    app = QApplication(sys.argv)
    myfirstgui=QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(myfirstgui)
    myfirstgui.show()
    sys.exit(app.exec_())

def get_params(rec):
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('--url', required=True, type=str,
                        help='Page address. For local files use the absolute path')
    parser.add_argument('--refresh', required=True, type=int,
                        help='Seconds between page reloads')
    parser.add_argument('--width', default=0.4, type=float,
                        help='Screen relative window width')
    parser.add_argument('--height', default=0.8, type=float,
                        help='Screen relative window height')
    parser.add_argument('--x', default=rec.width()*0.03, type=float,
                        help='X window location')
    parser.add_argument('--y', default=rec.height()*0.07, type=float,
                        help='Y window location')
    return parser.parse_args()

if __name__ == '__main__':
    app = QApplication([])
    gui = qtgui.qw.FoffaGui()
    gui.setup_ui('tripletrouble')
    gui.show()
    sys.exit(app.exec_())
