Foffa
=====

Windows Client for Xutt / alternative to Bronk


(Technical-) Design Decisons
----------------------------

* no edit functionality (incredible complex/not suited because of the dataset format) => you can edit the plain json files instead if you want
* cache datasets local
* update datasets on user wish, no auto-updates or auto-checks
* no self-updater, to get a new version you need to download it yourself (check for new version will be available however)
* dataset management, maybe poll a list of public datasets from a gitlab.io page, implement functionality to add or remove datasets from the ./data/ folder
* settings store as single json file in the workingdirectory, saves stuff like gitlab.io urls and possible hotkeyss