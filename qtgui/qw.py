#! /usr/bin/python3

# Foffa is a utility programm for text commanding GuildWars2.
# Copyright (C) 2017  Networkjanitor Netsphere
#
# This file is part of Foffa.
#
# Foffa is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Foffa is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Foffa. If not, see <http://www.gnu.org/licenses/>.

from PyQt5.QtCore import Qt, QRect
from PyQt5.QtWidgets import QPushButton, QHBoxLayout, QVBoxLayout, qApp, QScrollArea, QWidget, QTabWidget
import importlib
import pyperclip

class FoffaGui(QWidget):

    def setup_ui(self, switch_name):
        self.setWindowTitle('Foffa')
        self.resize(150, 500)

        #self.set_opacity()

        self.horizontalLayout = QHBoxLayout(self)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setSpacing(6)
        self.horizontalLayout.setObjectName("horizontalLayout")

        self.tabWidget = QTabWidget(self)
        self.tabWidget.setObjectName("tabWidget")

        # Control Tab
        self.control_tab = QWidget()
        self.control_tab.setObjectName("control_tab")

        self.quitButton = QPushButton('Quit', self.control_tab)
        self.quitButton.setGeometry(QRect(10, 10, 80, 22))
        self.quitButton.clicked.connect(self.quit)

        self.tabWidget.addTab(self.control_tab, "Control")

        # Other Stuff
        self.horizontalLayout.addWidget(self.tabWidget)

        self.choices = {}
        self.switch = None
        self._import_switch(switch_name)
        self.load_switch()

    def _import_switch(self, conf_name):
        conf_module = importlib.import_module("configurations.{}".format(conf_name))
        self.switch = conf_module.Switch()

    def set_opacity(self, opacity=0.5):
        """
        Makes window translucent.
        """
        self.setWindowOpacity(opacity)
        self.setAttribute(Qt.WA_TranslucentBackground)

    def quit(self):
        """
        Exit the application.
        """
        qApp.quit()

    def choice_made(self, key):
        current_conf = self.switch.tabs[self.tabWidget.currentIndex()-1]
        if current_conf.choices[key]['text'] is not None:
            pyperclip.copy(current_conf.choices[key]['text'])

    def load_switch(self):
        for tab in self.switch.get_tabinfos():
            self.add_tab(tab)

    def add_tab(self, conf):
        tab_name=conf.name
        nTab = QWidget()
        nTab.setObjectName(tab_name)

        # Container Widget        
        widget = QWidget()
        # Layout of Container Widget
        layout = QVBoxLayout(nTab)
        #layout.setContentsMargins(0,0,0,0)
        for c in conf.ordered_choices:
            btn = QPushButton(conf.choices[c]['title'])
            print(btn.styleSheet())
            btn.setStyleSheet("padding-left: 10px; padding-right: 10px; Text-align:left; ")
            btn.clicked.connect(lambda state, choice=c: self.choice_made(choice))
            layout.addWidget(btn)
        widget.setLayout(layout)

        # Scroll Area Properties
        scroll = QScrollArea()
        scroll.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        scroll.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        scroll.setWidgetResizable(False)
        scroll.setWidget(widget)

        # Scroll Area Layer add 
        vLayout = QVBoxLayout(nTab)
        vLayout.addWidget(scroll)
        nTab.setLayout(vLayout)

        self.tabWidget.addTab(nTab, tab_name)

